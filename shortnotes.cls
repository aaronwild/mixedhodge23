\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{shortnotes}[Shortnotes Class]

\LoadClass{scrartcl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%external packages needed for the class%% 
%(not the style)%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% colors (load before gitinfo2)
\RequirePackage[dvipsnames]{xcolor}

% header design
\RequirePackage[autooneside=false,automark]{scrlayer-scrpage}

%for the custom configuration
\RequirePackage{style-shortnotes}

%for patching 
\RequirePackage{etoolbox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%minimal configuration%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%layout
% Sets the page size
\KOMAoptions{paper = a4}

% Sets the font size
\KOMAoptions{fontsize = 11pt}

% Make the lines roughly 90 characters long
\KOMAoptions{DIV = 9}
%%
%%
%section-numbering
%
%only include section
\setcounter{tocdepth}{1}
%no labels for subsections
\setcounter{secnumdepth}{2}
%
%%
%section title formating 
\setkomafont{sectioning}{\rmfamily}
\addtokomafont{section}{\normalsize \centering \scshape}
\addtokomafont{subsection}{\itshape \normalsize \bfseries}
%%
%header configuration 
\pagestyle{plain}
\chead{}
\pagestyle{scrheadings}
\renewcommand*{\sectionmarkformat}{}
\newcommand{\ParagraphOrNot}{}
\chead{\ifodd\number\value{page}%
	\small\emph{\ParagraphOrNot\itshape{\rightmark}}%
\else%
	\small\emph{\ParagraphOrNot\itshape{\leftmark}}%
\fi%
}
%%
%begining of document 
\KOMAoptions{bibliography = totoc}
%distance of numbers in toc (?)
\renewcommand{\@pnumwidth}{2em}
%
%have to make weird things to get the title smaller 
\addtokomafont{title}{\rmfamily \bfseries}
%no spaces between section entries
\DeclareTOCStyleEntry[beforeskip=1pt]{section}{section}
%section entries in bf
\addtokomafont{sectionentry}{\bfseries}
\def\abstracttext#1{\def\@abstracttext{#1}}
\newcommand{\maketitlepage}{%
  {
  \thispagestyle{plain}
  \maketitle 
  \hypersetup{linkcolor=black}
  \hspace{.5cm}
  \vspace{-1.5cm}
  \begin{abstract}
   \footnotesize
    \@abstracttext
  \end{abstract}
	\tableofcontents%
  }
}
%%
%
%%
%citing
\usepackage[
	backend    = biber,
	style      = alphabetic,
	dateabbrev = false,
	urldate    = long,
  backref    = true,
  firstinits = true,
]{biblatex}


\newbibmacro*{bbx:parunit}{%
  \ifbibliography
    {\setunit{\bibpagerefpunct}\newblock
     \usebibmacro{pageref}%
     \clearlist{pageref}%
     \setunit{\adddot\par\nobreak}}
    {}}

\renewbibmacro*{doi+eprint+url}{%
  \usebibmacro{bbx:parunit}% Added
  \iftoggle{bbx:doi}
    {\printfield{doi}}
    {}%
  \iftoggle{bbx:eprint}
    {\usebibmacro{eprint}}
    {}%
  \iftoggle{bbx:url}
    {\usebibmacro{url+urldate}}
    {}}

\renewbibmacro*{eprint}{%
  \usebibmacro{bbx:parunit}% Added
  \iffieldundef{eprinttype}
    {\printfield{eprint}}
    {\printfield[eprint:\strfield{eprinttype}]{eprint}}}

\renewbibmacro*{url+urldate}{%
  \usebibmacro{bbx:parunit}% Added
  \printfield{url}%
  \iffieldundef{urlyear}
    {}
    {\setunit*{\addspace}%
     \printtext[urldate]{\printurldate}}}
\DeclareNameAlias{default}{family-given}
% always insert pp. after pagetotal,
% even if it isn’t recognized as a number by biblatex
\DeclareFieldFormat[book,booklet]{pagetotal}{#1~\ppno}

% no unplaned surprises with \cite
\DeclareFieldFormat{postnote}{#1}
\DeclareFieldFormat{multipostnote}{#1}

% formatting of entries
\DeclareFieldFormat[article,book,collection,incollection,proceedings,inproceedings]{number}{\mkbibbold{#1}}
\DeclareFieldFormat[article,collection,incollection]{volume}{\mkbibbold{#1}}
\DeclareFieldFormat[book]{pagetotal}{#1~\ppno}


%penalties
\displaywidowpenalty=8000
%\postdisplaypenalty=8000
\widowpenalty=8000
\clubpenalty=8000


%document lenghts
\newlength{\thmsep}%hax
\setlength{\thmsep}{\topsep}%dunno y dis workin
\setlength{\parindent}{1.5em}


%misc 

\RequirePackage{blindtext}
